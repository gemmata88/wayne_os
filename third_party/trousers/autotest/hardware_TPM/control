# Copyright (c) 2010 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

NAME = "TPM_TESTS"
AUTHOR = "The Chromium OS Authors"
PURPOSE = "Verify functionalit of system Trusted Platform Module."
CRITERIA = "This test is comprised of the Trousers Test Suite."
TIME = "LONG"
TEST_CATEGORY = "Functional"
TEST_CLASS = "Hardware"
TEST_TYPE = "Client"

DOC = """
    Tests various functionality of the TPM (Trusted Platform Module)
"""

test_suites = [
    'cmk',           #   ~6 tests: Certified Migratable Key functionality
    'context',       #  ~85 tests: Context objects
    'data',          #  ~16 tests: Data binding and sealing functionality
    'delegation',    #   ~1 tests: Key delegation
    'hash',          #  ~20 tests: Cryptographic hash functionality
    'key',           #  ~24 tests: Key objects
    'nv',            #  ~33 tests: NVRAM storage and use
    'pcrcomposite',  #  ~25 tests: PCR composites
    'policy',        #  ~13 tests: Policy objects
    'tpm',           # ~171 tests: Various types of low-level TPM functionality
    'transport',     # ~157 tests: Tests associated with transport sessions
    'tspi',          #  ~66 tests: TCG Service Provider Interface
]

owner_secret = None
if os.environ.has_key('TESTSUITE_OWNER_SECRET'):
    owner_secret = os.environ.get('TESTSUITE_OWNER_SECRET')

for suite in test_suites:
    job.run_test('hardware_TPM',
                 suite=suite,
                 owner_secret=owner_secret,
                 test_filter=None,
                 tag=suite)

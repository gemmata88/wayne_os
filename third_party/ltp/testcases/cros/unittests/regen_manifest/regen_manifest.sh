#!/bin/sh
# Copyright (c) 2013 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

# FILE:				regen_manifest.sh
#
# DESCRIPTION: Generate cros unittests test manifest file.
#
# AUTHOR:			 truty@chromium.org
#

MANIFEST=./cros_kernel_unittests.manifest

find ../../unittests/ -type f -name '*.c' -printf '%f\n' | \
	sed 's:[.]c$::' \
	> "${MANIFEST}"

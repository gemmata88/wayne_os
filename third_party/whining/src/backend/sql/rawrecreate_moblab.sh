#!/bin/bash
#
# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Drop and create the rawdb again with all the structure.
# rawdb acts as a cache between autotest and wmdb.

PWD="$1"
DB="${2:-x_rawdb}"
HOST="${3:-localhost}"

if [ -z "$PWD" ]; then
    echo "Usage: $0 password [database [host]]"
    exit 2
fi

MYSQL="mysql -u root -h $HOST -p$PWD"
echo "Dropping DB $DB"
echo "DROP DATABASE $DB" | $MYSQL

echo "Creating DB $DB"
echo "CREATE DATABASE $DB DEFAULT CHARACTER SET latin1" | $MYSQL

echo "Executing schema file"
$MYSQL $DB < rawdb_schema.sql
echo "Executing procs file"
$MYSQL $DB < rawdb_procs.sql
echo "Executing procs file"
$MYSQL $DB < rawdb_users.sql

#!/bin/bash
#
# Copyright 2015 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Drop and create a DB again with all the structure.
PWD="$1"
DB="${2:-x_wmdb}"
HOST="${3:-localhost}"

if [ -z "$PWD" ]; then
    echo "Usage: $0 password [database [host]]"
    exit 2
fi

MYSQL="mysql -u root -h $HOST -p$PWD"
# WARNING for CloudSQL there is no pwd
#MYSQL="/path/to/gsql user:db"
echo "Dropping DB $DB"
echo "DROP DATABASE $DB" | $MYSQL

echo "Creating DB $DB"
echo "CREATE DATABASE $DB DEFAULT CHARACTER SET latin1" | $MYSQL

echo "Executing schema file"
$MYSQL $DB < db_schema.sql
echo "Executing procs file"
$MYSQL $DB < db_procs.sql
echo "Executing views file"
$MYSQL $DB < db_views.sql
echo "Executing events file"
$MYSQL $DB < db_events.sql
echo "Executing users file"
$MYSQL $DB < db_users.sql

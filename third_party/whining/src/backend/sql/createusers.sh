#!/bin/bash
#
# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.
#
# Setup user passwords: this should be done before running [raw]recreate.sh,
# but this can also be run afterwards to reset the passwords.


#This whole section offends the minds of all right-thinking people. Check if two
# passwords are set, set another one to them (but only if they match), then set
# another pair of passwords to them. It's terrible. But it matches our usage,
# and this project is extremely deprecated.
# TODO(jkop): If it's past 2018 and this is still in use, kill it with fire.
if [ -n "$RAW_PASSWORD" ]
    PWD=$RAW_PASSWORD
fi
if [ -n "$WMATRIX_PASSWORD" ]
    PWD=$WMATRIX_PASSWORD
fi
if [ -n "$WMATRIX_PASSWORD" && -n "$RAW_PASSWORD" ]
    if [ "$WMATRIX_PASSWORD" -eq "$RAW_PASSWORD" ]
        PWD=$WMATRIX_PASSWORD
    else
        echo "RAW_PASSWORD and WMATRIX_PASSWORD must match if set"
        exit 2
    fi
fi

HOST=${1:-localhost}
MYSQL="mysql -u root -h $HOST -p$PWD"

if [ -z "$PWD" ]; then
    echo "Usage: $0 [host]"
    exit 2
else
    WMREADER_PASSWORD="$PWD"
    WMATRIX_PASSWORD="$PWD"
fi

if [ -z "$WMREADER_PASSWORD" -o -z "$WMATRIX_PASSWORD" ]; then
    # Reset tty echo on exit
    trap "[ -t 0 ] && stty echo" INT HUP TERM 0
    [ -t 0 ] && stty -echo
    echo -n "wmreader password: "
    read -r WMREADER_PASSWORD
    echo
    echo -n "wmatrix password: "
    read -r WMATRIX_PASSWORD
    echo

    if [ -z "$WMREADER_PASSWORD" -o -z "$WMATRIX_PASSWORD" ]; then
        echo "Please set both passwords."
        exit 1
    fi
fi

echo "Creating users:"
echo "CREATE USER 'wmreader'@'%'" | $MYSQL
echo "CREATE USER 'wmatrix'@'%'" | $MYSQL
echo "CREATE USER 'wmreader'@'localhost'" | $MYSQL
echo "CREATE USER 'wmatrix'@'localhost'" | $MYSQL

echo "Setting passwords:"
echo "SET PASSWORD FOR 'wmreader'@'%' = PASSWORD('$WMREADER_PASSWORD')" | $MYSQL
echo "SET PASSWORD FOR 'wmatrix'@'%' = PASSWORD('$WMATRIX_PASSWORD')" | $MYSQL
echo "SET PASSWORD FOR 'wmreader'@'localhost' = PASSWORD('$WMREADER_PASSWORD')" | $MYSQL
echo "SET PASSWORD FOR 'wmatrix'@'localhost' = PASSWORD('$WMATRIX_PASSWORD')" | $MYSQL
echo "FLUSH PRIVILEGES;" | $MYSQL

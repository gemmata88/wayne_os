%# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
%# Use of this source code is governed by a BSD-style license that can be
%# found in the LICENSE file.

%import datetime

%from src import settings
%_root = settings.settings.relative_root

%_mymax = lambda x: max(x) if x else ''

%# Method to create headers
%def toheader(header):
  {{ header.replace('_', ' ').title() }}
%end


%# Test result link with tooltip
%def test_links(test_name, suite, afe_job_id, idx, status):
  %if status.startswith('MISSING'):
    %return
  %end
  %_job_id = afe_job_id
  %_filter_tag = tpl_vars['filter_tag']
  %_link_props = [
  %    ('/testrun/%s?test_ids=%s' % (_filter_tag, idx),
  %     'Details', 'View test details.'),
  %    ('/testrun_log/%s?test_ids=%s' % (_filter_tag, idx),
  %     'Logs', 'View test logs.'),
  %    ('/%s?days_back=30&suites=%s&tests=%s' % (
  %         _filter_tag, suite, test_name),
  %     'History', 'View 30day test history.'),
  %    ('/reasons?suites=%s&tests=%s' % (
  %         suite, test_name),
  %     'Reasons', 'Frequencies of test failure reasons.'),
  %    ('/comments?test_ids=%s' % idx, 'Triage',
  %     'View/Add triage comments for test: %s in job: %s.' % (
  %         test_name, _job_id))]
  %for _href, _text, _tip in _link_props:
  <a onmouseover="fixTooltip(event, this)" onmouseout="clearTooltip(this)"
     href="{{ _root }}{{ _href }}" class="tooltip" target="_blank">
      {{ _text }}
    <span style="width:300px">{{ _tip }}</span>
  </a>
  %end
%end


%def body_block():
  %# --------------------------------------------------------------------------
  %# Releases switcher
  %include('switcher_bar.tpl', tpl_vars=tpl_vars, query_string=query_string, url_base='failures')

  %# --------------------------------------------------------------------------
  %# Failures
  <div id="divFailures" align="center">
  %_td = tpl_vars['data']['table_data']
  %_fields = tpl_vars['data']['fields']
  %if not _td.row_headers:
    <h3><u>No recent problems found.</u></h3>
  %else:
    <table style="width: 100%">
      <thead>
        <tr>
          <th class="headeritem centered"
              colspan={{ len(_td.col_headers) + 2 }}>
            Failures
          </th>
        </tr>
        <tr>
          <th class="headeritem centered">
            Action
          </th>
          <th class="headeritem centered">
            Count
          </th>
          %for _field in _fields:
            %if _field == 'reason':
            <th class="headeritem lefted">
            %else:
            <th class="headeritem centered">
            %end
              %toheader(_field)
            </th>
          %end
        </tr>
      </thead>
      <tbody>
        %for _row_key in _td.row_headers:
            %_test_name = _mymax(_td.get_cell(_row_key, 'test_name').data)
            %_afe_job_id = _mymax(_td.get_cell(_row_key, 'afe_job_id').data)
            %_idx = _mymax(_td.get_cell(_row_key, 'idx').data)
            %_status = _mymax(_td.get_cell(_row_key, 'status').data)
            %_suite = _mymax(_td.get_cell(_row_key, 'suite').data)
            <tr>
              <td class="failure_table">
                %test_links(_test_name, _suite, _afe_job_id, _idx, _status)
              </td>
              <td>
                {{ _td.get_cell(_row_key, 'test_name').count }}
              </td>
              %for _field in _fields:
                %_cell = _td.get_cell(_row_key, _field)
                <td class="failure_table">
                %if not _cell:
                  &nbsp;
                %else:
                  %_data = _mymax(_cell.data)
                  %if isinstance(_data, datetime.datetime):
                    {{ _data.strftime('%a %b %d, %H:%M') }}
                    %if len(_cell.data) > 1:
                      ...
                    %end
                  %elif len(_cell.data) > 1:
                    <div class="lefted">
                      <ul>
                      %for r in sorted(_cell.data):
                        %if _field == 'test_name':
                          <li>
                          <a href="{{ _root }}/failures/{{ tpl_vars['filter_tag'] }}{{! query_string(add={'tests': r} ) }}">
                            {{ r }}
                          </a>
                          </li>
                        %else:
                          <li>{{ r }}</li>
                        %end
                      %end
                      </ul>
                    </div>
                  %else:
                    %if _field == 'reason':
                      <div class="lefted">
                        {{ _data }}
                      </div>
                    %elif _field == 'auto_bug':
                      %if _data is not None:
                        <a href="http://crbug.com/{{_data}}">
                          crbug.com/{{ _data }}
                        </a>
                      %end
                    %elif _field in ['build', 'platform', 'suite', 'test_name']:
                      % _qs_key = _field.split('_')[0] + 's'
                      <a href="{{ _root }}/failures/{{ tpl_vars['filter_tag'] }}{{! query_string(add={_qs_key: _data} ) }}">
                        {{ _data }}
                      </a>
                    %else:
                      {{ _data }}
                    %end
                  %end
                %end
                </td>
              %end
            </tr>
        %end
      </tbody>
    </table>
  %end
  </div>
  <hr>
%end

%rebase('master.tpl', title='failures', query_string=query_string, body_block=body_block)

# Copyright 2018 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

description     "The setup and teardown logic for hardening the lock screen."
author          "allenwebb@chromium.org"

# Prior to starting usbguard-daemon, the rule files in
# "/etc/usbguard/rules.d/" are concatenated in alphabetical order to
# create the runtime rules configuration at "/run/usbguard/rules.conf".
# Filename prefixes are used to control the order of the included files.
# Here are the prefixes
#   00- is reserved for a default header for all boards.
#   {10..29}- are for high priority rules.
#   {30..59}- are for default priority rules.
#   {60..79}- are for low priority rules.
#   99- is reserved for the default footer for all boards.
# Most board specific configurations should use the "50-" prefix.

# These signals are emitted by session_manager.
#
# screen-unlocked may not be called for every screen-locked event, but
# start-user-session covers those cases.
start on screen-locked
stop on (screen-unlocked or start-user-session) and stopped usbguard

pre-start script
  if ! /usr/share/cros/init/is_feature_enabled.sh 'USBGuard'; then
    logger -t "${UPSTART_JOB}" 'USBGuard disabled.'
    stop
    exit 0
  fi

  logger -t "${UPSTART_JOB}" 'Locking USB.'
  # White-list connected devices.
  umask 077
  mkdir -p /run/usbguard/
  cd /run/usbguard/
  rm -f rules.conf

  if /usr/share/cros/init/is_feature_enabled.sh 'USBBouncer'; then
    # TODO(b/926974) Use sed to replace the final allow rule with block
    # to change from a blocklisting scheme to a allowlisting scheme.
    # This is temporary until the USB Bouncer feature is no longer
    # controlled by Finch.
    logger -t "${UPSTART_JOB}" 'USB Bouncer enabled.'
    /usr/sbin/usb_bouncer genrules | sed 's/^allow$/block/' > rules.conf
  else
    logger -t "${UPSTART_JOB}" 'USB Bouncer disabled.'
    # "usbguard generate-policy" queries the connected devices to
    # create a list of rules that are used to allow-list already
    # connected devices so they are not blocked if power management
    # turns them off and back on while at the lock screen. The USB
    # device descriptors are untrusted input parsed by USBGuard, so it
    # is a good idea to use sandboxing even though the daemon is not
    # being executed. See usbguard.conf for the choice of minijail flags.
    /sbin/minijail0 -u usbguard -g usbguard -c 2 -l -p -n -e --uts \
        --profile=minimalistic-mountns \
        -b /sys,,1 \
        -S /usr/share/policy/usbguard-daemon-seccomp.policy \
        /usr/bin/usbguard generate-policy 2>&1 >rules.conf | \
        logger -e -t "${UPSTART_JOB}"
    # Wireless mice may not be authorized but still need to be allow-listed.
    sed -i 's/^block/allow/' rules.conf
    find /etc/usbguard/rules.d -type f -print0 | sort -z | \
      xargs -0 cat >> rules.conf
  fi
  chown usbguard:usbguard . rules.conf
end script

post-stop script
  rm -f /run/usbguard/rules.conf
  logger -t "${UPSTART_JOB}" "Unlocking USB."

  cd /sys/devices
  # A timeout is used to skip an operation if it is taking an unexpectedly long
  # time. 2.1 seconds was chosen because the kernel will print a warning after
  # 2 seconds, so this choice allows the warning to be logged.
  cmd='for F; do timeout 2.1 sh -c "echo 1 > \"\${0}\"" "${F}" || :; done'
  # Accept new usb devices by default.
  find . -name authorized_default -exec sh -c "${cmd}" - {} +
  # Authorize connected devices that were not authorized.
  find . -name authorized -exec sh -c "${cmd}" - {} +
end script

# Copyright 2019 The Chromium OS Authors. All rights reserved.
# Distributed under the terms of the GNU General Public License v2

EAPI=6

DESCRIPTION="Generic ebuild which satisfies virtual/chromeos-dtc-vm
which is used as part of the diagnostics package."

LICENSE="BSD"
SLOT="0"
KEYWORDS="*"
